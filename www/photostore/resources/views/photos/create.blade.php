@extends('layouts.app')

@section('content')

<script type='text/javascript'>
    function preview_image(event) 
    {
     var reader = new FileReader();
     reader.onload = function()
     {
      var output = document.getElementById('output_image');
      output.src = reader.result;
     }
     reader.readAsDataURL(event.target.files[0]);
    }
</script>


<div class="container bg-white border border-secondary">
    <div class="d-flex justify-content-center border-bottom border-secondary py-4">
        <h1 class="font-weight-bold">Ajouter une photo</h1>
    </div>

    <div class="row mt-5 pb-10">
        <form action="/photos" method="post" enctype="multipart/form-data" class="col-12 mt-3 mx-auto row d-flex justify-content-around">
            @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                Le formulaire contient des erreurs
            </div>
            @endif

            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
            @endif

            @csrf
            <div class="col-sm-4">
                <div class="mt-5">
                    <a action="{{ route('image.upload.post') }}" method="POST" enctype="multipart/form-data">
                        <div class="text-center">
                            <input type="file" name="url_photo" class="form-control mb-2" onchange="preview_image(event)">  
                            <img id="output_image" style="width: 340px; height: 250px;">
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-sm-4 column mt-5 font-weight-bold">
                <div class="form-group row d-flex justify-content-start">
                    <div class="mb-3">
                        <label class="mt-1" for="titre">Titre</label>
                    </div>
                    <div class="offset-2">
                        <input type="text" class="form-control @error('titre') is-invalid @enderror" id="titre" name="titre" 
                        value="{{ old('titre') }}">
                        @error('titre')
                            <span class="text-danger help-block">{{ $message }} </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row d-flex justify-content-start">
                    <div class="mb-3">
                        <label class="mt-1" for="mots_cles">Mots-clés</label>
                    </div>
                    <div class="offset-1">
                        <input class="form-control @error('mots_cles') is-invalid @enderror" id="mots_cles" name="mots_cles">{{ old('mots_cles') }}</input>
                        @error('mots_cles')
                            <span class="text-danger help-block">{{ $message }} </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row d-flex justify-content-start">
                    <div>
                        <label class="mt-1" for="categories">Categorie</label>
                    </div>
                    <select name="categories" id="photos" class="offset-1">
                        <option value="nature">Nature</option>
                        <option value="animaux">Animaux</option>
                        <option value="ville">Ville</option>
                    </select>
                </div>
                <div class="text-center mt-5">    
                    <button type="submit" class="btn btn-default bg-dark text-white font-weight-bold">Envoyer</button>
                </div>
            </div>  
        </form>
    </div>
</div>

@endsection