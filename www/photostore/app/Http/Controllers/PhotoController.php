<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Photo;
use Auth;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Recuperer tout le photo e stocker dans une variable
        $photos = Photo::all();
        //envoyer le donnee vers la vue index.blade
        return view('photos.index')->with('photos' , $photos);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('photos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file('url_photo');
        $imagename = time() . '_' .$request->file('url_photo')->getClientOriginalName();
         
        
        $imagePath= public_path('/images/');
        $image->move($imagePath, $imagename);
 
        $photo = new Photo($request->all());
        $photo->url_photo = '/images/'.$imagename;
        $photo->save();

        $user = Auth::user();
        $user->solde = $user->solde + 5;
        $user->nombre_uploads++;
        $user->save();
        // Rediriger vers la page d'accueil
        return redirect ('/photos');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //retrouver la photo en function de l'id passè en parametre
        $photo = Photo::find('id');

        // Injecter la photo dans la page detail
        // generer la page detail.blade-php e la retourner
        return view('photos.detail')->with('photo', $photo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function imageUpload()
    {
        return view('imageUpload');
    }

    public function download($file_name) {
        
        $file_path = public_path('images/'.$url_photo);
        
        return response()->download($file_path);
    }
}
