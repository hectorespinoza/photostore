<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
        'titre',
        'mots_cles',
        'categories',
        'url_photo'

    ];
}
