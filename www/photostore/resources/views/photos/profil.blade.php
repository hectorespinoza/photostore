@extends('layouts.app')

@section('content')
<div class="container bg-white border border-secondary">
    <div class="text-center border-bottom border-secondary py-4">
        <h1 class="font-weight-bold">Profil</h1>
    </div>
    <div class="row text-center marginProfil py-10 d-flex justify-content-around">
        <div class="col-sm-3 border border-secondary py-4">
            <p class="mb-5 textProfil">Téléchargements</p>
            <a>
                <svg class="bi bi-caret-down-fill mb-5 text-success" width="3.5em" height="3.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path d="M7.247 11.14L2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 01.753 1.659l-4.796 5.48a1 1 0 01-1.506 0z"/>
                </svg>
            </a>
            <p class="textProfil border-bottom border-secondary w-50 mx-auto">
            {{ $user->nombre_telechargements }} 
            </p>
        </div>
        <div class="col-sm-3 border border-secondary py-4">
            <p class="mb-5 textProfil">Uploads</p>
            <a>
                <svg class="bi bi-caret-up-fill mb-5 text-primary" width="3.5em" height="3.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path d="M7.247 4.86l-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 00.753-1.659l-4.796-5.48a1 1 0 00-1.506 0z"/>
                </svg>
            </a>
            <p class="textProfil border-bottom border-secondary w-50 mx-auto">
                {{ $user->nombre_uploads }}
            </p>
        </div>
        <div class="col-sm-3 border border-secondary py-4">
            <p class="mb-5 textProfil">Solde</p>
            <a>
                <svg class="bi bi-gem mb-5 text-warning" width="3.5em" height="3.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M3.1.7a.5.5 0 01.4-.2h9a.5.5 0 01.4.2l2.976 3.974c.149.185.156.45.01.644L8.4 15.3a.5.5 0 01-.8 0L.1 5.3a.5.5 0 010-.6l3-4zm11.386 3.785l-1.806-2.41-.776 2.413 2.582-.003zm-3.633.004l.961-2.989H4.186l.963 2.995 5.704-.006zM5.47 5.495l5.062-.005L8 13.366 5.47 5.495zm-1.371-.999l-.78-2.422-1.818 2.425 2.598-.003zM1.499 5.5l2.92-.003 2.193 6.82L1.5 5.5zm7.889 6.817l2.194-6.828 2.929-.003-5.123 6.831z" clip-rule="evenodd"/>
                </svg>
            </a>
            <p class="textProfil border-bottom border-secondary w-50 mx-auto">
                {{ $user->solde }}
            </p>
        </div>
    </div>
</div>

@endsection