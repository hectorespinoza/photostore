@extends('layouts.app')

@section('content')

<div class="container border border-secondary bg-white">
    <div class="row p-2 font-weight-bold">
        <div class="ml-3">
            <label for="photos" class="mt-2 mr-2">Filter par : Categorie</label>
            <select id="photos" class="col-5">
                <option value="nature">Nature</option>
                <option value="animaux">Animaux</option>
                <option value="ville">Ville</option>
            </select>
        </div>

        <div class="offset-1">   
            <ul class="navbar-nav mr-auto">
                <form class="form-inline" action="/photos/search" method="POST">
                    @csrf
                    <div class="row">
                        <label type="text" class="mr-2">{{ __('MOTS CLES') }}</label>
                        <input type="text" class="form-control" name="search" placeholder="">
                    </div>
                </form>
            </ul> 
        </div>
    </div>        
</div>

<div class="container border border-secondary mt-1 bg-white">
    <div class="row mt-5">
        @foreach($photos as $photo)
        <div class="col-sm-4 font-weight-bold mb-5">
            <img class="img-thumbnail" style="width: 350px; height: 275px;" src="{{ asset("$photo->url_photo") }}" />
            <div class="row d-flex justify-content-around"> 
                <span class="mt-2 ">{{ $photo->titre }}</span> 
                <span class="d-flex mt-2">{{ $photo->created_at }}</span> 
                <a href="" class="text-dark">   <!-- TODO: href pour le download de la photo. --> 
                    <svg class="bi bi-download mt-2" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M.5 8a.5.5 0 01.5.5V12a1 1 0 001 1h12a1 1 0 001-1V8.5a.5.5 0 011 0V12a2 2 0 01-2 2H2a2 2 0 01-2-2V8.5A.5.5 0 01.5 8z" clip-rule="evenodd"/>
                        <path fill-rule="evenodd" d="M5 7.5a.5.5 0 01.707 0L8 9.793 10.293 7.5a.5.5 0 11.707.707l-2.646 2.647a.5.5 0 01-.708 0L5 8.207A.5.5 0 015 7.5z" clip-rule="evenodd"/>
                        <path fill-rule="evenodd" d="M8 1a.5.5 0 01.5.5v8a.5.5 0 01-1 0v-8A.5.5 0 018 1z" clip-rule="evenodd"/>
                    </svg>
                </a>  
            </div>
        </div>
        @endforeach
    </div>
</div>

 @endsection